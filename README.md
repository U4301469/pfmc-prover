# Parallel Open Source Fixedpoint Model Checker (PFMC) 

_Version: 2022-01-10_

This is a modified version of the Open Source Fixedpoint Model Checker (OFMC) 2020 version. The modification adds support for multicore execution of the model checker. 

See the accompanying technical report (report.pdf) for the high-level design of the prover. 

## Compiling PFMC

This project was configured using the Haskell Stack build environment to ensure reproducibility. The program runs in an isolated container so it will not interfere with any existing Haskell installation. To build this project, you need a recent Haskell Stack installed. This was tested using Stack version 2.5.1. 

In the following, we assume a Linux based installation and compilation (eg, Ubuntu 20.04). To install stack, run:
```bash
$ sudo apt install haskell-stack 
```
Then run
```bash 
$ stack upgrade
```
to upgrade your installation. Make sure you update your PATH enviroment variable so that the directory $HOME/.local/bin is in your PATH. This will ensure the latest version of stack is used when you do the next steps below.

To compile the PFMC program, run the following commands in the pfmc directory:
```bash
$ stack setup
$ stack build
```
Normally these will automatically download the required version of ghc and packages. 
Depending on your stack installation, stack may not download the `alex` and the `happy` programs, which are a lexer and a parser generator, respectively. In that case, run 
```bash
$ stack build alex happy
```
and repeat the stack setup and build commands above. 

To install the program, run
```bash
$ stack install
```
This will install the executable file `pfmc-exe` to `$HOME/.local/bin`. Make sure that that directory is in your PATH environment. 


## Running pfmc

Currently parallel execution is supported only for the classic bounded model checking algorithm, and only on input files using the AnB syntax. PFMC currently implements 12 parallelisation strategies. There are three options, each takes an integer argument (denoted by `N` below), that control the mode of parallelisation in PFMC:

- `--treeStrat N`: choose tree strategy `N` to use in the search. When not specified, the default value of `N` is 0. 

- `--bufferSize N`: spefiy a `buffer size` of `N` to use with a strategy. The `buffer size` is interpreted differently, depending on the strategy used. When not specified, the default value of `N` is 5. 

- `--chunkSize N`: speficy a `chunk size` of `N` to use with a strategy. This is mainly relevant for chunking strategies (see below) and is not used in some strategies (e.g., `parTreeBuffer`). When not specified, the default value of `N` is 3.

The list of strategies is as follows (see the file `src/TreeStrategies.hs` for their implementations): 

- Strategy 0: `parTreeBuffer`. This strategy requires a parameter specifying the `par depth` (which controls, roughly, how deeper the sparking is done, from the current node), which can be controlled using the `--bufferSize` option. It is very similar to `parTreeBufferOriginal` but it evaluates the spine of the list of children nodes.  

- Strategy 1: `parTreeChunkSubtrees`. Use with `--chunkSize N`. Evaluates at most `N` subtrees in parallel. 

- Strategy 2: `parTreeChunk`. Use with option `--chunkSize N`. Calls `parListChunk` with chunk size `N` at every level. 

- Strategy 3: `chunkNEvalTree`. Use with option `--chunkSize N`. Once the number of children gets below the chunk size `N`, spark the entire subtree. 

- Strategy 4: `parTree`. Basic parTree. Sparks the entire tree -- use with caution! 

- Strategy 5: `parTreeChunkBuffer`. Use with options  `--chunkSize M` and `--bufferSize N`. Combines chunking and buffering at every level.

- Strategy 6: Not used. 

- Strategy 7:  `parTreeChildren`. One spark for each node's children.

- Strategy 8: `hybridSubtrees`. Use with option `--chunkSize N`. Same as parTreeBuffer until the fuel runs out then sparks the subtree.

- Strategy 9: `depthSubtrees`. Use with option `--chunkSize N`. Use `parBuffer` to a certain depth then spark the remaining subtree.

- Strategy 10: `parTreeChunkBuffer2`. Use with options `--chunkSize M` and `--bufferSize N`. 

- Strategy 11: `evalTree rpar`. Similar to `parTree`. 

- Strategy 12: `parTreeBufferOriginal`. Use with option `--bufferSize N`. Similar to `parTreeBuffer`.


In addition to the choice of strategies (and its associated parameter(s)), you also need to specify both the number of sessions and the depth of search. For example, to check for a protocol specified in the file 'test.AnB', for 2 sessions, with search depth 10, using strategy `parTreeBuffer` and 4 CPU cores, run the following command (assuming `pfmc-exe` is installed in the path of the current user):

```bash
pfmc-exe test.AnB --numSess 2 --depth 10 --treeStrat 0 --bufferSize 50 +RTS -s -N4
```

Some example protocols are provided in the `examples/` directory. 

> **IMPORTANT NOTE:** The option `--depth 10` must be specified in order to trigger the parallel code in PFMC. If it is not specified, the prover defaults to the sequential implementation in OFMC. 


## Using custom algebraic operators (experimental)

The original OFMC allows one to specify custom algebraic operators and their equational theory using the `--theory` option. Currently this option is only effective if the input file is in the IF format. PFMC implemented an experimental feature that allows limited custom operators to be used in the AnB syntax. 
There are two custom operators currently available for use: `custOp1` and `custOp2`. Both of these have no
theories defined in the code; you will have to define them manually. Below is a list of instructions on how to use this extension. 

1. Write an AnB specification using `custOp1` or `custOp2` as the operator, e.g. here is an example of a protocol step that uses `custOp1`:

    ````
    R->T: R1,R2, custOp1(custOp1(R1,R1),R2)
    ````

2. Convert the AnB file into IF using pfmc with the following command:

    ````
    pfmc-exe --of IF test.AnB > test.If
    ````

    At this point, `pfmc` may give an error regarding situations where it believes that it is impossible for a process to know a message, e.g. R1 is never known by R. This may occur in cases where the message would have been known by the process if the algebraic properties were used, but without the algebraic properties, pfmc thinks that there is a problem. In such a case, there is no current alternative except to modify the protocol to avoid the issue, translate it to IF, and then manually edit the IF code back to what you originally wanted. This is not as much trouble as writing the IF file from scratch, if you can translate a protocol that is very close to what you want.

3. Specify your algebraic theory in a theory file. Use the file `examples/alg/stdCustOp.thy` as an example. In that file, we have
copied the xor definition of `theories/std.thy` and replaced that with `CustOp1`. Note that the operator name should start with a
lower-case inside the theory definition, but with an upper case in the heading of the theory.

4. Run pfmc with your algebraic theory and the If file, such as:

    ````
    pfmc-exe --theory stdCustOp.thy test.If --depth 10 --treeStrat 0 --bufferSize 50 +RTS -s -N4 
    ````




## License

The original version of the OFMC software is under the BSD License. See the included `LICENSE` file. This modification for the parallel execution was done by Alex James, Alwen Tiu and Nisansala Yatapanage. 

