{-

Open Source Fixedpoint Model-Checker version 2020

(C) Copyright Sebastian Moedersheim 2003,2020
(C) Copyright Paolo Modesti 2012
(C) Copyright IBM Corp. 2009
(C) Copyright ETH Zurich (Swiss Federal Institute of Technology) 2003,2007

All Rights Reserved.

-}

module Search where

import IntsOnly
import Remola
import Symbolic
import NewIfParser
import Data.Maybe
import Data.List
import Constants
import Decomposition
import TreeStrategies

-- [AT] for debugging and parallel evaluation
import Data.Tuple
import Debug.Trace
import Control.Parallel
import Control.Parallel.Strategies as ParS 
-- import Control.Monad.Par as ParM 
import Control.DeepSeq 
import GHC.Float (float2Double)
import GHC.RTS.Flags (GCFlags(stkChunkSize))

varstart=221


getmaxstep :: [(Int -> Rule)] -> [(Msg,Int)] -> Bool -> ([(Msg,Int)],Bool)
getmaxstep [] n b = (n,False)
getmaxstep (f:fs) n b = 
  let (_,_,_,r,_) = f 0
      step = map (\ (W step _ _ m _ _) -> (m,step)) 
                 (filter (\ fact -> 
                           case fact of 
                           (W _ _ _ _ _ _) -> True
                           _ -> False) r)
      loop (m,s) ([],b) = ([(m,s)],b && (s>0))
      loop (m,s) (((m',s'):ms),b) = 
         if m==m' 
         then if s>s' then (((m,s):ms),b) else (((m,s'):ms),True)
         else let (ms',b') = (loop (m,s) (ms,b))
	      in ((m',s'):ms',b')
      (job,b') = foldr loop (n,False) step
  in getmaxstep fs job b'

{-
-- [AT] a strategy for parallel evaluation of state trees. 
evalTree  :: Int -> Strategy a -> Strategy (Tree a)
evalTree 0  strat t = return t 
evalTree !n strat (Node x l) = do 
  y <- strat x 
  l' <- parList rseq (map (withStrategy (evalTree (n-1) strat)) l)
  return (Node y l')
-}

type MonState = (Int,State) 
--- a monadic state, i.e. State and a counter for the variables.

successor :: AlgTheo -> Bool -> Int -> [(Int -> Rule)] -> MonState -> [MonState]
--- sfb: bound on the session repetitions
--- rules: a set of rules parametrized over the time point (this
---        timepoint is not necessary in future versions).
--- state: the current state (with monad)
--- result: successor states (with monads)

successor theo por sfb rules (seed,state) =
  zip (repeat seed) (concatMap (\rule -> successors very_lazy theo por sfb rule state) rules) 


-- [AT] parallel version
{-
-- Turned out the successor produces at most 1 successor, at least for some examples
-- So this is not a productive use of parMap 
successor theo por sfb rules (seed,state) =
  let l = parMap rseq (\rule -> successors very_lazy theo por sfb rule state) rules
      r = (concat l)
      z = zip (repeat seed) r 
  in 
    if (length z > 0) then trace ("successor " ++ (show (length z))) z else z
-}

successorHC :: [Hornclause] -> AlgTheo -> Bool -> Int -> [(Int -> Rule)] -> MonState -> [MonState]
--- sfb: bound on the session repetitions
--- rules: a set of rules parametrized over the time point (this
---        timepoint is not necessary in future versions).
--- state: the current state (with monad)
--- result: successor states (with monads)
successorHC hcs theo por sfb rules (seed,state) =
  zip (repeat seed) (concatMap (\rule -> successorsHC hcs very_lazy theo por sfb rule state) rules) 

-- [AT] parallel version
-- successorHC hcs theo por sfb rules (seed,state) =
--    let l = ParS.parMap rseq (\rule -> successorsHC hcs very_lazy theo por sfb rule state) rules
--    in zip (repeat seed) (concat l)


successor_must :: AlgTheo -> Bool -> Int -> [(Int -> Rule)] -> MonState -> [MonState]
successor_must theo por sfb rules (seed,state) =
  case concatMap (\ rule -> successors_must theo por sfb rule state) rules of
  [] -> successor theo por sfb rules (seed,state)
  x  -> [(seed, head x)]

wellformed :: String -> Bool
wellformed errmsg = (prefix "secrecy" errmsg) || (prefix "authen" errmsg)

prefix [] _ = True
prefix (x:xs) (y:ys) = x==y && (prefix xs ys)
prefix _ _ = False

general_attack_check :: AlgTheo ->  Bool -> [(Int -> Rule)] -> State -> Maybe String
general_attack_check theo por attack_rules state =
  let attack_states = successor theo False 707 attack_rules (1000,state)
  in if attack_states == [] then Nothing else
     let attack_state --- @(b,t,f,oi,ni,po,cs,h)
           = snd (head attack_states) 
         (W _ _ errmsg _ _ _) = head (filter isWTerm (facts attack_state))
     in
      Just (prlist (show errmsg) attack_state)

type Succ_rel a = (Int,a) -> [(Int,a)]


treeconstruct0 :: NFData a => Int -> Succ_rel a -> (Int,a) -> Tree a
--- depth: bound on the tree-size
--- rules,state: as above.
--- output tree of states
treeconstruct0 0     succ_rel (seed,state) = Node state []
treeconstruct0 depth succ_rel mstate =  
--  Node (snd mstate) (ParS.parMap rdeepseq (treeconstruct0 (depth-1) succ_rel) (succ_rel mstate))
  Node (snd mstate) (map (treeconstruct0 (depth-1) succ_rel) (succ_rel mstate))

treeconstruct :: [Hornclause] -> AlgTheo -> Bool -> Bool ->  Int -> Int -> (State,[Int->Rule]) -> Tree State
--- given sfb and depth as above
--- and an if-description
--- output: tree of states
treeconstruct hcs theo must por sfb depth (state0,rules) = 
  treeconstruct0 depth ((if must then error "successor_must theo" else successorHC hcs theo) por sfb rules) (varstart, state0)
  --  `using` evalTree depth 10 rseq   --[AT] evaluate the search tree in parallel 
  --  `using` evalTree5 depth 4 rseq   --[AT] evaluate the search tree in parallel 

wrids :: Int -> (State -> Bool) -> Tree State -> (String,Int)
wrids 0 p (Node a l) = if p a then ("Lalala"++(prlist "?" a),0) else ([],1)
wrids n p (Node a l) = if p a then ("Lalala"++(prlist "?" a)++rest,nds) else (rest,nds)
      where (rest,nds) = foldr (\ (s1,i1) (s2,i2) -> (s1++s2,i1+i2)) 
			       ("",0)
			       (map (wrids (n-1) p) l)


{-

-- [AT] original code
wrids2 :: Int -> (State -> Maybe String) -> Tree State -> (String,Int)
wrids2 0 p (Node a l) = 
   let isAttack =  (p ( a)) in
   if (isJust isAttack) then ('*':(fromJust isAttack),1) else (nothing_found,1)
wrids2 !n p (Node a l) =
   let isAttack =  (p ( a)) in
   if (isJust isAttack) then ('*':(fromJust isAttack),nds) else (rest,nds)
      where (rest,nds) = foldr ( \ (s1,i1) (s2,i2) ->  
                                     case s1 of 
                                       '*':s1' -> (s1,i1)
                                       _ -> (s2,i1+i2)
                               )
                               ("",0)
                               (map (wrids2 (n-1) p) l)

-}


-- [AT] Modified parallel search
wrids2 :: Int -> Int -> Int -> (State -> Maybe String) -> Tree State -> Int -> (String,Int)
wrids2 chunkSize bufferSize n p t treeStrat = extractTrace (pruneTree n (\x -> p x) t `using` parEvalTree chunkSize bufferSize n treeStrat)

-- [AT] a strategy for parallel evaluation of state trees. 
-- The actual implementation of the strategy is in TreeStrategies module.
parEvalTree  :: NFData a => Int -> Int -> Int -> Int -> Strategy (Tree a)
parEvalTree chunkSize bufferSize n treeStrat
  | treeStrat == 0  = parTreeBuffer bufferSize rseq                           -- Original parTreeBuffer with optimisation by [AT]
  | treeStrat == 1  = parTreeChunkSubtrees rseq chunkSize                     -- Evaluates at most d subtrees in parallel.
  | treeStrat == 2  = parTreeChunk chunkSize rseq                             -- Calls parListChunk d at every level.
  | treeStrat == 3  = chunkNEvalTree chunkSize                                -- Subtrees of size d.
  | treeStrat == 4  = parTree rseq                                            -- Basic parTree.
  | treeStrat == 5  = parTreeChunkBuffer chunkSize bufferSize rseq            -- Chunking + buffering at every level.
  | treeStrat == 7  = parTreeChildren rseq                                    -- One spark for each node's children.
  | treeStrat == 8  = hybridSubtrees rseq chunkSize                           -- Same as parTreeBuffer until the fuel runs out then sparks the subtree.
  | treeStrat == 9  = depthSubtrees chunkSize                                 -- parBuffer to a certain depth then spark the remaining subtree.
  | treeStrat == 11 = evalTree rpar
  | treeStrat == 10 = parTreeChunkBuffer2 chunkSize bufferSize rseq
  | treeStrat == 12 = parTreeBufferOriginal bufferSize bufferSize rseq
  | otherwise      = rseq

-- [AT] extract a trace from a search tree
extractTrace :: Tree (Maybe String) -> (String, Int)
extractTrace (Node x []) =
  if (isJust x) then ('*':fromJust x,1) else (nothing_found,1)
extractTrace (Node x l) =
  if (isJust x) then ('*':fromJust x,nds) else (rest,nds)
      where (rest,nds) = foldr
                          ((\ (s1, i1) (s2, i2)
                              -> case s1 of
                                  '*' : s1' -> (s1, i1)
                                  _ -> (s2, i1 + i2))
                            . extractTrace)
                          ("", 0) l
                               -- (parMap rseq extractTrace l)

type Statistics = (Int,Int)
--- 1st int: vip --- the number of nodes visited
--- 2nd int: depth -- maximum depth visited
init_stats = (0,0)
inc_vip (vip,depth) = (vip+1,depth)
get_vip (vip,_) = vip
inc_depth (vip,depth) = (vip,depth+1)
max_depth (vip,depth) depth' = (vip,max(depth,depth'))
get_depth (_,depth) = depth

show_stats :: Statistics -> String
show_stats (vip,depth) = if vip==0 then "" else
                         "  visitedNodes: "++(show vip)++" nodes\n"
                         ++"  depth: "++(show depth)++" plies\n"

neusuch :: AlgTheo -> Bool -> Bool -> [Int -> Rule] -> [Hornclause] -> (State -> Maybe String) -> Int -> Int -> Int ->
   ([(Int,State)],[(Int,State)]) -> Statistics -> (String,Statistics)
--- given a rule set, an attack predicate, a max. number of states in
--- memory, a max. depth bound and a list of trees. Perform breadth-first search
--- until reaching the max. number, then proceed with depth first search.

nothing_found = "No attacks found.\n"
nothing_found_bound = "No attacks found.%(reached depth restriction)\n"
reached_balance = "neusuch: reached balance.\n"

neusuch theo must por rules hcs predicate balance 0 _ _ stats
 = (nothing_found,stats) --- was: nothing_found_bound
neusuch theo must por rules hcs predicate balance depth sfb ([],[]) stats
 = (nothing_found,stats)
neusuch theo must por rules hcs predicate balance depth sfb ([],l) stats
 = neusuch theo must por rules hcs predicate balance (depth-1) sfb (reverse l,[]) 
   (inc_depth stats)
neusuch theo must por rules hcs p balance depth sfb (x:xs,ys) stats
 = let isAttack = (p (snd x)) in
   if (isJust isAttack) then (fromJust isAttack, stats) 
   else let succ_x = if must then error "MUST CURRENTLY not supported" else --- successor_must theo else 
   	    	     successorHC hcs theo por sfb rules x 
            isAttack = (listToMaybe . catMaybes . (map (p . snd))) succ_x
        in if (isJust isAttack) then (fromJust isAttack,stats) else
	   if (((length succ_x) < balance))
	   then neusuch theo must por rules  hcs p (balance-(length succ_x)+1) depth sfb 
		   (xs, (reverse succ_x) ++ ys) (inc_vip stats)
	   else (reached_balance,stats)


exec_check0 :: AlgTheo -> Bool -> [Int -> Rule] -> (State -> Maybe [Fact]) -> Int -> Int -> Int ->
   ([(Int,State)],[(Int,State)]) -> Statistics -> (Maybe [Fact],Statistics)

exec_check0 theo por rules predicate balance 0 _ _ stats
 = error "Executability check stopped due to bounds." 
exec_check0 theo por rules predicate balance depth sfb ([],[]) stats
 = (Nothing,stats)
exec_check0 theo por rules predicate balance depth sfb ([],l) stats
 = exec_check0 theo por rules predicate balance (depth-1) sfb (reverse l,[]) 
   (inc_depth stats)
exec_check0 theo por rules p balance depth sfb (x:xs,ys) stats
 = let isAttack = (p (snd x)) in
   if (isJust isAttack) then (isAttack, stats) 
   else let succ_x = (successor theo por sfb rules x) 
            isAttack = (listToMaybe . catMaybes . (map (p . snd))) succ_x
        in if (isJust isAttack) then (isAttack,stats) else
	   if (((length succ_x) < balance))
	   then exec_check0 theo por rules p (balance-(length succ_x)+1) depth sfb 
		   (xs, (reverse succ_x) ++ ys) (inc_vip stats)
	   else 
	    error "exec_check0: reached balance. Please report this bug."

partition_by_session :: [Fact] -> [[Fact]]
partition_by_session [] = [[]]
partition_by_session (x:xs) = 
        let (same_session,other_session) = 
		partition ((==) (get_session x).get_session) xs
        in ((x:same_session):(partition_by_session other_session))

get_session :: Fact -> Int
get_session  (W _ _ _ _ _ (Number session)) = session 



xexec_check :: AlgTheo -> Bool -> [Int -> Rule] -> [(Msg,Int)] -> (Int,State) -> (Bool,[Fact])
xexec_check theo por rules final_states (a,initial_state) =
--- @(a,(b,c,initial_facts,d,e,f,g,h)) =
  let (state_facts,other_facts) = partition isWTerm (facts initial_state)
      state_partition = partition_by_session state_facts
      analysis = (map (\ (c,session_facts) -> 
		exec_check0 theo por rules (\ state -> check_states state final_states) 
		  100000 100000 100000 
		  ([(a,initial_state{time=c,
				     facts=session_facts++other_facts})],[])
		  init_stats) (zip (map ((*)(length rules)) [1..n]) state_partition))
      n = length state_partition
      (sessco,remains) = partition isJust (map fst analysis)
  in if (remains==[])
     then (True,(concatMap fromJust sessco))
     else (False,(concatMap fromJust sessco))


check_states :: State -> [(Msg,Int)] -> Maybe [Fact]
check_states state [] = 
  Just (history state)
check_states state dings@((m,i):mis) =
  let not_finished = filter (\ f -> case f of 
			              (W i' _ _ m' _ _) -> (i>i') && (m'==m)
				      _ -> False) (facts state) in
  if (not_finished==[]) && (isJust (check_states state mis))
  then Just ((filter (not . isWTerm) (facts state))++(history state))
  else Nothing




shn2 :: (State -> Maybe String) -> (Int -> Tree State) -> [Int] -> String 
shn2 p tree l = 
  let state = (top (mytraverse (tree ((length l)+1) ) l)) in
    (showState3 state)
        ++ "\n" ++ (printniveau (mytraverse (tree ((length l)+1) ) l)) ++ "\n"

sprintTree :: Tree State -> String
sprintTree tree = printniveau2 "" tree


top :: Tree a -> a
top (Node a _) = a

children :: Tree a -> [Tree a]
children (Node _ l) = l

nth :: Tree a -> Int -> Tree a
nth t n = nthl (children t) n

nthl [] _ = error "Ply to short"
nthl (x:_) 0 = x
nthl (_:xs) n = nthl xs (n-1)

nprint :: State -> String
nprint state = (if (porflag state) then "***" else "")++(show (history state))

nprintt :: Tree State -> String
nprintt (Node a l) = nprint a

nprintwl :: State -> String
nprintwl = show . init . history

nprinttwl :: Tree State -> String
nprinttwl (Node a l) = nprintwl a

nprintl :: [Fact] -> State -> String
nprintl lab state =
   let consume [] l = l
       consume (x:xs) (y:ys) = consume xs ys
   in  print_trace  (consume lab (history state))

print_trace :: [Fact] -> String
print_trace [] = ""
print_trace (x:xs) = (show x)++"\n"++(print_trace xs)

nprinttl :: [Fact] -> Tree State -> String
nprinttl lab (Node a l) = nprintl lab a

printniveau :: Tree State -> String
printniveau (Node state l) = 
  let showalt [] i = ""
      showalt (x:xs) i = 
        "("++(show i)++")\n"++(nprinttl (history state) x)++"\n"++(showalt xs (i+1))
  in 
     "\n"++(print_trace (history state))++"\n"++(showalt l 0)++"\n"

printniveau2 :: String -> Tree State -> String
printniveau2 indent (Node state l) = 
  let showalt [] i = ""
      showalt (x:xs) i = 
        indent++"("++(show i)++") "++(nprinttl2 (history state) x)++
	(printniveau2 (indent++"|") x)++(showalt xs (i+1))
  in 
  showalt l 0

nprintl2 :: [Fact] -> State -> String
nprintl2 lab state =
   let consume [] l = l
       consume (x:xs) (y:ys) = consume xs ys
   in  print_trace2 (consume lab (history state))

print_trace2 :: [Fact] -> String
print_trace2 [] = ""
print_trace2 (x:xs) = (show x)++(print_trace xs)

nprinttl2 :: [Fact] -> Tree State -> String
nprinttl2 lab (Node a l) = nprintl2 lab a

mytraverse :: Tree a -> [Int] -> Tree a
mytraverse t [] = t
mytraverse t (x:xs) = mytraverse (nth t x) xs

len (Node _ l) = length l






