{-

Strategies for the evaluation of unbounded trees in parallel.

(c) 2021-2022 by Alex James and Alwen Tiu


-}

module TreeStrategies (
    -- The tree type with unbounded branching.
    Tree (..),              
    parTree,                -- NFData a => Strategy a -> Strategy (Tree a)
    evalTree,               -- NFData a => Strategy a -> Strategy (Tree a)
    pruneTree,              -- NFData a => Int -> (a -> b) -> Tree a -> Tree b
    parTreeBuffer,          -- NFData a => Int -> Strategy a -> Strategy (Tree a)
    parTreeChunkSubtrees,   -- NFData a => Strategy a -> Int -> Strategy (Tree a)
    parTreeChunk,           -- NFData a => Int -> Strategy a -> Strategy (Tree a)
    chunkNEvalTree,         -- NFData a => Int -> Strategy (Tree a)
    parTreeChunkBuffer,     -- NFData a => Int -> Int -> Strategy a -> Strategy (Tree a)
    parTreeChildren,        -- NFData a => Strategy a -> Strategy (Tree a)
    hybridSubtrees,         -- NFData a => Strategy a -> Int -> Strategy (Tree a)
    parTreeChunkBuffer2,    -- NFData a => Int -> Int -> Strategy a -> Strategy (Tree a)
    depthSubtrees,          -- NFData a => Int -> Strategy (Tree a)
    parTreeBufferOriginal   -- Int -> Int -> Strategy a -> Strategy (Tree a)
 ) where

import Debug.Trace
import Control.Parallel
import Control.Parallel.Strategies as ParS 
import Control.DeepSeq 

{- Data Definitions -}
-- Defining our Tree.
data Tree a = Node a [Tree a]

-- An annotated tree in which each node contains the number of nodes in its subtree.
data CTree = CNode Int [CTree]

-- [AT] Definining our Tree as an instance of NFData.
-- This is to denote that it is a data type that can be fully evaluated.
instance NFData a => NFData (Tree a) where 
  rnf (Node x l) = rnf x `seq` rnf l 

{- Tree Strategies -}

-- Evaluate a tree according to a given strategy.
-- This should be replaced by making Tree traversable.
evalTree :: NFData a => Strategy a -> Strategy (Tree a)
evalTree strat (Node x []) = do
    y <- strat x
    return (Node y [])
evalTree strat (Node x l) = do
    y <- strat x
    l' <- evalList (evalTree strat) l
    return (Node y l')

-- [AT] an simple parallel strategy for trees.  
-- IMPORTANT NOTE: parTree assumes the input tree is well-founded (no infinite branch), so every branch terminates
-- at a leave node of the form (Node x []). So if the original tree is not depth bounded, it
-- needs to be transformed to a depth-bounded tree, e.g, using the pruneTree function above. 
-- [AT] Caution is recommended using this strategy, as the memory consumption grows exponentially. 
parTree :: Strategy a -> Strategy (Tree a)
parTree strat (Node x l) = do 
  y <- strat x 
  cl <- rseq (length l)
  l' <- parList (parTree strat) l 
  return (Node y l')

-- [AT] Original parTreeBuffer.
-- Worse than parTreeBuffer, kept as legacy.
parTreeBufferOriginal :: Int -> Int -> Strategy a -> Strategy (Tree a)
parTreeBufferOriginal _ _ strat (Node x []) = do 
  y <- strat x 
  return (Node y [])
parTreeBufferOriginal 0 n strat (Node x l) = do 
  y <- strat x
  -- Evaluate the elements of l to WHNF. parBuffer uses 'withStrategy' to compose the given strategy
  -- It creates a spark but evaluates the expression to WHNF only, so does not recurse through the tree.  
  l' <- parBuffer 50 (parTreeBufferOriginal n n strat) l  
  return (Node y l')
parTreeBufferOriginal m n strat (Node x l) = do 
  y <- strat x 
  -- [AT] eagerly spark evaluations of subtrees. parList uses rparWith for each element, so a new spark  is created
  --      for each element of the list. 
  l' <- parList (parTreeBufferOriginal (m-1) n strat) l   
  return (Node y l')

-- [AT] Enhanced parTreeBuffer.
-- Lazily evaluates in parallel by calling parBuffer at each node. 
parTreeBuffer :: Int -> Strategy a -> Strategy (Tree a)
parTreeBuffer _ strat (Node x []) = do 
  y <- strat x 
  return (Node y [])
parTreeBuffer bufferSize strat (Node x l) = do 
  y <- strat x 
  n <- rseq (length l) 
  l' <- parBuffer bufferSize (parTreeBuffer bufferSize strat) l
  return (Node y l')

-- Sparks at most a given number of subtrees concurrently.
-- Extremely memory efficient with reasonable speedup, but does not scale as well.
parTreeChunkSubtrees :: NFData a => Strategy a -> Int -> Strategy (Tree a)
parTreeChunkSubtrees strat fuel (Node x [])                     
    -- If there are no children, use any remaining fuel to evaluate the node then finish.
    | fuel >= 1 = do
        y <- rpar x
        return (Node y [])
    | otherwise = do
        y <- strat x
        return (Node y [])
parTreeChunkSubtrees strat fuel (Node x l)    
    -- With less fuel than children, create a buffer with that fuel and evaluate the remaining subtrees.        
    -- Because a buffer is used, there will never be more sparks than fuel at the same time.          
    | fuel <= numChildren = do                                  
        y <- strat x                                            
        l' <- parBuffer fuel rdeepseq l                     
        return (Node y l')
    -- If there is enough fuel to share, distribute a portion to each child.
    | otherwise = do                                            
        y <- strat x
        return (Node y evaluatedChildren)
  where
    evaluatedChildren = applyEach (customFuelSplit (parTreeChunkSubtrees strat) remainder portion numChildren) l
    numChildren = length l
    portion = fuel `div` numChildren
    remainder = mod fuel numChildren

-- Chunks at every level. Use with caution, high memory consumption and overhead.
-- Likely only applicable for very fine grained problems and high branching factors.
parTreeChunk :: NFData a => Int -> Strategy a -> Strategy (Tree a)
parTreeChunk _ strat (Node x []) = do
    y <- strat x
    return (Node y [])
parTreeChunk n strat (Node x l) = do
    y <- strat x
    l' <- parListChunk n (parTreeChunk n strat) l
    return (Node y l')

-- Creates one spark for each list of children.
-- Chunks more effectively than parTreeChunk.
parTreeChildren :: NFData a => Strategy a -> Strategy (Tree a)
parTreeChildren strat (Node x []) = do
    y <- strat x
    return (Node y [])
parTreeChildren strat (Node x l) = do
    y <- strat x
    l' <- rparWith (evalList (parTreeChildren rseq)) l
    return (Node y l')

-- An initial attempt at a hybrid chunk-buffer strategy. Ineffective.
parTreeChunkBuffer :: NFData a => Int -> Int -> Strategy a -> Strategy (Tree a)
parTreeChunkBuffer _ _ strat (Node x []) = do
    y <- strat x
    return (Node y [])
parTreeChunkBuffer n b strat (Node x l) = do
    y <- strat x
    l' <- parListChunkBuffer n (parTreeChunkBuffer n b strat) b l
    return (Node y l') 

-- Once the number of children gets below the chunk size, spark the entire subtree.
-- Until then, use parBuffer with chunkSize. Ineffective, likely due to poor approach to annotation.
chunkNEvalTree :: NFData a => Int -> Strategy (Tree a)
chunkNEvalTree chunkSize tree = chunkTrees chunkSize rseq (countBranching tree) tree

chunkTrees :: NFData a => Int -> Strategy a -> CTree -> Strategy (Tree a)
chunkTrees _ strat _ (Node x []) = do
    y <- strat x
    return (Node y [])
chunkTrees chunkSize strat (CNode cx cl) (Node x l) 
    -- If the number of subnodes is less than the chunk size, then spark the remaining subtrees with parBuffer.
    | cx <= chunkSize = do                                        
        y <- strat x
        l' <- parBuffer cx (evalTree rdeepseq) l
        return (Node y l')
    -- If not, keep going down the tree.
    | otherwise = do                                              
        y <- strat x
        l <- parBuffer chunkSize rseq l
        return (Node y (applyEach annChunkSplit l))
    where annChunkSplit = pairAnnotations (chunkTrees chunkSize strat) cl l
          -- Pass the annotation info to the right subtree strategy.
          pairAnnotations :: NFData a => (CTree -> Strategy (Tree a)) -> [CTree] -> [Tree a] -> [Strategy (Tree a)]
          pairAnnotations treeStrat ctrees [] = []
          pairAnnotations treeStrat (c:cs) (t:ts) = thisStrategy : nextStrategies
            where
              thisStrategy = treeStrat c
              nextStrategies = pairAnnotations treeStrat cs ts

-- Use parBuffer until the fuel runs out, then spark the subtree.
hybridSubtrees :: NFData a => Strategy a -> Int -> Strategy (Tree a)
hybridSubtrees strat fuel (Node x [])
  | fuel >= 1 = do
    y <- rpar x
    return (Node y [])
  | otherwise = do
    y <- rseq x
    return (Node y [])
hybridSubtrees strat fuel (Node x l)
    | fuel == 0 = do
      y <- strat x
      l' <- evalBuffer 50 (evalTree rseq) l
      return (Node y l')
    -- With less fuel than children, create a buffer with that fuel and evaluate the remaining subtrees.
    -- Because a buffer is used, there will never be more sparks than fuel at the same time.
    | fuel <= numChildren = do                                  
      y <- strat x                                              
      l' <- parBuffer fuel rdeepseq l
      return (Node y l')
    -- If there is enough fuel to share, divide it between the children.
    | otherwise = do                                            
        y <- strat x
        cl <- rseq (length l)
        l' <- customParBuffer 50 childStrats l
        return (Node y l')
  where 
    numChildren = length l
    remainder = (fuel - numChildren) `mod` numChildren
    portion = (fuel - numChildren) `div` numChildren
    childStrats = customFuelSplit (hybridSubtrees strat) remainder portion numChildren

-- A lower-level approach to combining chunking and buffering for trees.
-- Strangely inconsistent performance. Most of the time all the sparks will be GC'd.
parTreeChunkBuffer2 :: NFData a => Int -> Int -> Strategy a -> Strategy (Tree a)
parTreeChunkBuffer2 _ _ strat (Node x []) = do
    y <- strat x
    return (Node y [])
parTreeChunkBuffer2 chunkSize bufferSize strat (Node x l) = do
    y <- strat x
    cl <- rseq (length l)
    l' <- parListChunkBuffer2 chunkSize bufferSize (parTreeChunkBuffer2 chunkSize bufferSize strat) l
    return (Node y l')

-- A lower-level approach to combining chunking and buffering for lists.
parListChunkBuffer2 :: NFData a => Int -> Int -> Strategy (Tree a) -> Strategy [Tree a]
parListChunkBuffer2 c b strat = parChunkBufferTreeWHNF c b . map (withStrategy strat)

-- A modification of parBufferWHNF which also chunks the list.
parChunkBufferTreeWHNF :: NFData a => Int -> Int -> Strategy[Tree a]
parChunkBufferTreeWHNF c b xs0 = return (ret xs0 (start b xs0))
  where ret :: NFData a => [Tree a] -> [Tree a] -> [Tree a]
        ret xs ys
          | length xs >= c = (visitChildren c ys `using` rdeepseq) `par` (take c xs ++ ret (drop c xs) (drop c ys))
          | otherwise = ys `par` xs

        start :: NFData a => Int -> [Tree a] -> [Tree a]
        start 0   ys     = ys
        start n []     = []
        start n  ys 
          | length ys >= c = (visitChildren c ys `using` rdeepseq) `par` start (n-1) (drop c ys)
          | otherwise = ys `par` start (n-1) (drop c ys)

        -- Visit each child in a given list of children.
        visitChildren :: NFData a => Int -> [Tree a] -> [Tree a]
        visitChildren c [] = []
        visitChildren 0 children = children
        visitChildren c (child:children) = visitNode child : visitChildren (c - 1) children

        -- Eagerly evaluate just this node's value.
        visitNode :: NFData a => NFData a => Tree a -> Tree a
        visitNode (Node x l) = Node (x `using` rseq) l

-- parBuffer to a certain depth then spark the remaining subtree.
depthSubtrees :: NFData a => Int -> Strategy (Tree a)
depthSubtrees d = depthSubtrees' d 0

depthSubtrees' :: NFData a => Int -> Int -> Strategy (Tree a)
depthSubtrees' td d (Node x [])
  | td > d = do
    y <- rpar x
    return (Node y [])
  | otherwise = do
    y <- rseq x
    return (Node y [])
depthSubtrees' td d (Node x l)
  -- If the target depth is reached, create a spark to fully evaluate the remaining subtree.
  | td == d = rparWith rdeepseq (Node x l)
  -- If not, create a buffer and recursively call the strategy with depth + 1.
  | td > d  = do
    y <- rseq x
    cl <- rseq (length l)
    l' <- parBuffer 50 (depthSubtrees' td (d + 1)) l
    return (Node y l')

{- Utility Functions -}
-- [AT] A prune / map function for trees, prunes a tree based on tree height.
pruneTree :: Int -> (a -> b) -> Tree a -> Tree b 
pruneTree 0 f (Node x l) = Node (f x) []
pruneTree n f (Node x l) = Node (f x) (map (pruneTree (n-1) f) l)

-- Distribute a given amount of fuel across a list of tree strategy applications.
-- This is used to evenly distribute fuel to children, without losing any to whole number division.
-- The first 'remainder' children get +1 fuel.
customFuelSplit :: NFData a => (Int -> Strategy (Tree a)) -> Int -> Int -> Int -> [Strategy (Tree a)]
customFuelSplit treeStrat remainder portion 0 = []
customFuelSplit treeStrat remainder portion numChildren = thisStrategy : nextStrategies
  where 
    thisStrategy = treeStrat thisSparks
    nextStrategies = customFuelSplit treeStrat (remainder - 1) portion (numChildren - 1)
    thisSparks = do
      if remainder == 0
        then portion
        else portion + 1

-- A custom parBuffer strategy that allows a different strategy to be applied to each element in the buffer.
customParBuffer :: Int -> [Strategy a] -> Strategy [a]
customParBuffer n strats = parBufferWHNF n . applyEach strats

-- parBufferWHNF is copied from Control.Parallel.Strategies, as it is not exported from that library.
parBufferWHNF :: Int -> Strategy [a]
parBufferWHNF n0 xs0 = return (ret xs0 (start n0 xs0))
  where -- ret :: [a] -> [a] -> [a]
           ret (x:xs) (y:ys) = y `par` (x : ret xs ys)
           ret xs     _      = xs

        -- start :: Int -> [a] -> [a]
           start 0   ys     = ys
           start n []     = []
           start n  (y:ys) = y `par` start (n-1) ys

-- Apply a list of strategies to a list.
applyEach :: [Strategy a] -> [a] -> [a]
applyEach _ [] = []
applyEach [] _ = []
applyEach (strat:strats) (target:targets) = withStrategy strat target : applyEach strats targets

-- Combines parListChunk and parBuffer.
parListChunkBuffer :: Int -> Strategy a -> Int -> Strategy [a]
parListChunkBuffer n strat buff xs
  | n <= 1    = parList strat xs
  | otherwise = concat `fmap` parBuffer buff (evalList strat) (chunk n xs)

-- Copied from Control.Parallel.Strategies for use in parListChunkBuffer (Not exported by that library).
chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n xs = as : chunk n bs where (as,bs) = splitAt n xs

{- These utility functions are used in tree annotation. -}
-- Create an annotated tree from a tree.
countBranching :: NFData a => Tree a -> CTree
countBranching (Node x []) = CNode 0 []
countBranching (Node x l) = CNode c cs
    where 
        countChildren ((CNode gcc gc):children) = 1 + gcc + countChildren children
        countChildren [] = 0

        cs = map countBranching l
        c = countChildren cs

-- Return the total children for an annotated tree.
totalBranches :: CTree -> Int
totalBranches (CNode c l) = c

-- Return the total number of children for a tree.
totalChildren :: NFData a => Tree a -> Int
totalChildren tree = totalBranches $ countBranching tree
